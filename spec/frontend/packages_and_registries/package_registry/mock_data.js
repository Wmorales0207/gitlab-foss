export const packageTags = () => [
  { id: 'gid://gitlab/Packages::Tag/87', name: 'bananas_9', __typename: 'PackageTag' },
  { id: 'gid://gitlab/Packages::Tag/86', name: 'bananas_8', __typename: 'PackageTag' },
  { id: 'gid://gitlab/Packages::Tag/85', name: 'bananas_7', __typename: 'PackageTag' },
];

export const packagePipelines = (extend) => [
  {
    commitPath: '/namespace14/project14/-/commit/b83d6e391c22777fca1ed3012fce84f633d7fed0',
    createdAt: '2020-08-17T14:23:32Z',
    id: 'gid://gitlab/Ci::Pipeline/36',
    path: '/namespace14/project14/-/pipelines/36',
    name: 'project14',
    ref: 'master',
    sha: 'b83d6e391c22777fca1ed3012fce84f633d7fed0',
    project: {
      name: 'project14',
      webUrl: 'http://gdk.test:3000/namespace14/project14',
      __typename: 'Project',
    },
    user: {
      name: 'Administrator',
    },
    ...extend,
    __typename: 'Pipeline',
  },
];

export const packageFiles = () => [
  {
    id: 'gid://gitlab/Packages::PackageFile/118',
    fileMd5: null,
    fileName: 'foo-1.0.1.tgz',
    fileSha1: 'be93151dc23ac34a82752444556fe79b32c7a1ad',
    fileSha256: null,
    size: '409600',
    __typename: 'PackageFile',
  },
  {
    id: 'gid://gitlab/Packages::PackageFile/119',
    fileMd5: null,
    fileName: 'foo-1.0.2.tgz',
    fileSha1: 'be93151dc23ac34a82752444556fe79b32c7a1ss',
    fileSha256: null,
    size: '409600',
    __typename: 'PackageFile',
  },
];

export const packageData = (extend) => ({
  id: 'gid://gitlab/Packages::Package/111',
  name: '@gitlab-org/package-15',
  packageType: 'NPM',
  version: '1.0.0',
  createdAt: '2020-08-17T14:23:32Z',
  updatedAt: '2020-08-17T14:23:32Z',
  status: 'DEFAULT',
  ...extend,
});

export const conanMetadata = () => ({
  packageChannel: 'stable',
  packageUsername: 'gitlab-org+gitlab-test',
  recipe: 'package-8/1.0.0@gitlab-org+gitlab-test/stable',
  recipePath: 'package-8/1.0.0/gitlab-org+gitlab-test/stable',
});

export const composerMetadata = () => ({
  targetSha: 'b83d6e391c22777fca1ed3012fce84f633d7fed0',
  composerJson: {
    license: 'MIT',
    version: '1.0.0',
  },
});

export const pypyMetadata = () => ({
  requiredPython: '1.0.0',
});

export const mavenMetadata = () => ({
  appName: 'appName',
  appGroup: 'appGroup',
  appVersion: 'appVersion',
  path: 'path',
});

export const nugetMetadata = () => ({
  iconUrl: 'iconUrl',
  licenseUrl: 'licenseUrl',
  projectUrl: 'projectUrl',
});

export const packageDetailsQuery = () => ({
  data: {
    package: {
      ...packageData(),
      metadata: {
        ...conanMetadata(),
        ...composerMetadata(),
        ...pypyMetadata(),
        ...mavenMetadata(),
        ...nugetMetadata(),
      },
      tags: {
        nodes: packageTags(),
        __typename: 'PackageTagConnection',
      },
      pipelines: {
        nodes: packagePipelines(),
        __typename: 'PipelineConnection',
      },
      packageFiles: {
        nodes: packageFiles(),
        __typename: 'PackageFileConnection',
      },
      __typename: 'PackageDetailsType',
    },
  },
});

export const emptyPackageDetailsQuery = () => ({
  data: {
    package: {
      __typename: 'PackageDetailsType',
    },
  },
});
